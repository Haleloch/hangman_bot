const Discord = require('discord.js');
const settings = require('./config.json');
const $ = require('cheerio');
const rp = require('request-promise');
const client = new Discord.Client();

client.login(settings.token);

const lang = {
	'arabic': 'kalimat-eashwayiya',
	'catalan': 'index',
	'danois': 'tilfaeldige-ord',
	'eng': 'random-words',
	'finn': 'satunnaiset-sanat',
	'fr': 'mots-aleatoires',
	'german': 'zufallige-worter',
	'dutch': 'willekeurige-woorden',
	'italian': 'parole-casuali',
	'portuguese': 'palavras-aleatorias',
	'spanish': 'index',
	'swedish': 'slumpade-ord'
};

color = {
	'critical': '000000',
	'error': 'ffffff',
	'fail': 'd64118',
	'game': '2b7fb3',
	'help': 'bdbfbd',
	'succeed': '45c20c',
	'warning': 'f78b1e'
};

games = {};

client.on('ready', () => {
	console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', msg => {
	if (!msg.author.bot) {
		if (msg.content == settings.prefix + settings.command + " help") {
			let out = "Afin de lancez votre partie, vous pouvez simplement taper : `" + settings.prefix + settings.command + '`.\n';
			out += "Vous pouvez aussi jouer à plusieurs en mentionnant les personnes avec qui vous souhaitez jouer.\n"
			out += "Vous aurez le choix de choisir la langue, pour cela lors du lancement de la partie, vous pouvez préciser : "
			for (var key in lang) {
				out += "\n`" + key + "`"
			}
			msg.channel.send(new Discord.MessageEmbed()
				.setColor(color['help'])
				.setTitle(`Page d'aide pour le jeu ${settings.command}`)
				.setDescription(out));
		} else if (msg.content == settings.prefix + settings.command + " stop") {
			let check = checkGame(msg);

			if (check.key != -1) {
				msg.channel.send(new Discord.MessageEmbed()
					.setColor(color['game'])
					.setTitle(`Fin de partie`)
					.setDescription('Votre partie a été supprimée.'))
				delete games[check.key];
			}
		}
		else if (msg.content.startsWith(settings.prefix + settings.command)) {
			// prefix+command language @Mentions
			// ex: *hangman fr @Haleloch
			const args = msg.content.slice(settings.prefix.length).trim().split(/ +/g);
			let language = 'fr'; //Define default language
			args.forEach(arg => {
				if (Object.keys(lang).indexOf(arg) != -1) {
					language = arg;
				}
			});

			let key = new Date().getTime();
			let players = [];
			games[key] = { players: [], word: '', game: '', errors: [] };
			players.push(msg.author.id);
			msg.mentions.members.forEach(mention => {
				players.push(mention.id);
			});

			let check = checkPlayer(players);
			if (check.key == -1) {
				chooseWord(language).then(word => {
					if (word != -1) {
						games[key].players = players;
						games[key].word = word;
						games[key].game = init(word);
						msg.channel.send(new Discord.MessageEmbed()
							.setColor(color['game'])
							.setTitle(`Lancement d'une nouvelle partie`)
							.setDescription('Voici le mot à trouver:\n```' + games[key].game.split('').join(' ') + '```'));
					}
				}).catch(() => {
					console.log(Date() + " : Error, no word has been chosen")
					console.log("Trace : " + msg.author.username + "` in `" + msg.guild.name + "`");
					msg.channel.send(new Discord.MessageEmbed()
						.setColor(color['critical'])
						.setTitle(`Problème !!`)
						.setDescription("Un problème est survenu lors de la création de la partie.\nVeuillez contacter l'administrateur de votre serveur ou directement le développeur <@621333786432634880>"))
				});
			} else {
				if (check.players.indexOf(msg.author.id) != -1 && check.players.length == 1) {
					error = "Vous êtes déjà inscrit dans une partie !!";
				} else if (check.players.indexOf(msg.author.id) != -1) {
					error = " Vous ainsi que";
					msg.mentions.members.forEach(mention => {
						if (check.players.indexOf(mention.id) != -1) {
							error += " " + mention.displayName + ",";
						}
					});
					error = error.substring(0, error.length - 1) + " êtes déjà dans une partie !";
				} else {
					error = (check.players.length > 1 ? "Les joueurs" : "Le joueur");
					check.players.forEach(player => {
						msg.mentions.members.forEach(mention => {
							if (mention.id == player) {
								error += " " + mention.displayName + ",";
							}
						});
					});
					error = error.substring(0, error.length - 1) + " ";
					error += (check.players.length > 1 ? "sont déjà inscrits" : "est déjà inscrit") + " dans une partie :cry:"
				}

				msg.channel.send(new Discord.MessageEmbed()
					.setColor(color['error'])
					.setTitle(`Erreur lors du lancement d'une partie`)
					.setDescription(error));
			}

		} else if (msg.content == "errors") {
			let check = checkGame(msg);

			if (check.key != -1) {
				msg.channel.send(new Discord.MessageEmbed()
					.setColor(color['game'])
					.setTitle(`Liste des lettres fausses`)
					.setDescription(display_errors(games[check.key].errors)));
			}
		} else if (msg.content == "show") {
			let check = checkGame(msg);

			if (check.key != -1) {
				msg.channel.send(new Discord.MessageEmbed()
					.setColor(color['game'])
					.setTitle(`Affichage du mot à trouver`)
					.setDescription('Votre mot actuel:\n```' + games[check.key].game.split('').join(' ') + '```'));
			}
		} else {
			let check = checkPlayer(msg.author.id);

			if (check.key != -1 && msg.content.length == 1) {
				let tmp = games[check.key].game;
				let tmp2 = games[check.key].errors.length;
				let out = "";
				games[check.key].game = discover(games[check.key].word, games[check.key].game, is_present(check.key, msg.content));

				if (tmp == games[check.key].game && tmp2 == games[check.key].errors.length) {
					msg.channel.send(new Discord.MessageEmbed()
						.setColor(color['game'])
						.setTitle(`Lettre déjà saisie`)
						.setDescription('Attention vous avez déjà saisi cette lettre !!'));
				} else if (tmp == games[check.key].game) {
					let lost = false;
					out += display_errors(games[check.key].errors) + "\n" + '```' + hangman[games[check.key].errors.length] + '```';
					if (games[check.key].errors.length + 1 == hangman.length) {
						lost = true;
						out += `\nPerdu, le mot à trouver était ** ${games[check.key].word} **`;
						delete games[check.key];
					}
					msg.channel.send(new Discord.MessageEmbed()
						.setColor(color[lost ? 'fail' : 'warning'])
						.setTitle(`Votre lettre n'était pas présente dans le mot`)
						.setDescription(out));
				} else {
					out += 'Votre mot actuel:\n```' + games[check.key].game.split('').join(' ') + '```';
					msg.channel.send(new Discord.MessageEmbed()
						.setColor(color['game'])
						.setTitle(`Bravo, un pas de plus vers la victoire`)
						.setDescription(out));
				}

				if (games[check.key] && games[check.key].word == games[check.key].game) {
					msg.channel.send(new Discord.MessageEmbed()
						.setColor(color['succeed'])
						.setTitle(`Partie gagnée !!`)
						.setDescription(`Félicitations, vous avez trouvé le mot ** ${games[check.key].word} **`));
					delete games[check.key];
				}
			}
		}
	}
});

function checkPlayer(users) {
	let players = [];
	let out = { "key": -1, "players": -1 };

	for (var key in games) {
		games[key].players.forEach(player => {
			if (users.indexOf(player) != -1) {
				out.key = key;
				players.push(player);
			}
		})
	}
	out.players = players;

	return out;
}

function checkGame(msg) {
	let check = checkPlayer(msg.author.id);

	if (check.key == -1) {
		msg.channel.send(new Discord.MessageEmbed()
			.setColor(color['error'])
			.setTitle(`Erreur : Aucune partie trouvée`)
			.setDescription("Nous sommes désolé mais vous n'êtes inscrit dans aucune partie :cry:"));
	}

	return check;
}

async function chooseWord(language) {
	const url = 'https://www.palabrasaleatorias.com/' + lang[language] + '.php?fs=1&fs2=0';
	await rp(url).then(function (html) {
		word = $('div[style="font-size:3em; color:#6200C5;"]', html)[0].childNodes[0].data.replace(/(\r\n|\n|\r)/gm, "");
	});

	return word;
}

function is_present(key, letter) {
	let word = games[key].word;
	let index_arr = [];
	word = word.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase();
	letter = letter.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase();

	let tmp = index = 0;
	while (index != -1) {
		index = word.indexOf(letter);
		if (index != -1) {
			tmp += 1 + index;
			index_arr.push(tmp - 1);
			word = word.substring(index + 1);
		}
	}

	if (index_arr.length == 0 && games[key].errors.indexOf(letter) == -1) {
		games[key].errors.push(letter);
	}

	return index_arr;
}

function init(word) {
	let out = '';
	for (var i = 0; i <= word.length; i++) {
		let letter = word[i];
		if (letter == ' ' || letter == '-') {
			out += letter;
		} else {
			out += '_';
		}
	};

	out = out.substring(0, out.length - 1);
	return out;
}

function discover(word, hangman, arr) {
	let out = '';
	for (var i = 0; i < hangman.length; i++) {
		if (arr.indexOf(i) != -1) {
			out += word[i];
		} else {
			out += hangman[i];
		}
	};

	return out;
}

function display_errors(errors) {
	if (errors && errors.length) {
		out = 'Voici la liste des lettres incorrectes : ';
		errors.sort()
		for (let i = 0; i < errors.length; i++) {
			out += errors[i] + (i + 1 != errors.length ? ', ' : '');
		}
	} else {
		out = 'Vous n\'avez pas encore donné de lettres non présentes dans le mot.';
	}
	return out;
}


const hangman = [];
hangman[0] = "";
hangman[1] = "           \n           \n           \n           \n             \n     \n    \n \n============____________________________";
hangman[2] = "   ||        \n   ||        \n   ||        \n   ||        \n   ||          \n   ||  \n   ||  \n   ||\n============____________________________";
hangman[3] = "   ||        \n   ||        \n   ||        \n   ||        \n   ||          \n   ||  \n  /||  \n //||\n============____________________________";
hangman[4] = "   ,==========Y===\n   ||        \n   ||        \n   ||        \n   ||        \n   ||          \n   ||  \n  /||  \n //||\n============____________________________";
hangman[5] = "   ,==========Y===\n   ||  /      \n   || /       \n   ||/        \n   ||        \n   ||          \n   ||  \n  /||  \n //||\n============____________________________";
hangman[6] = "   ,==========Y===\n   ||  /      |\n   || /       |\n   ||/        \n   ||        \n   ||          \n   ||  \n  /||  \n //||\n============____________________________";
hangman[7] = "   ,==========Y===\n   ||  /      |\n   || /       |\n   ||/        O\n   ||        \n   ||          \n   ||  \n  /||  \n //||\n============____________________________";
hangman[8] = "   ,==========Y===\n   ||  /      |\n   || /       |\n   ||/        O\n   ||         |\n   ||          \n   ||  \n  /||  \n //||\n============____________________________";
hangman[9] = "   ,==========Y===\n   ||  /      |\n   || /       |\n   ||/        O\n   ||        /|\n   ||          \n   ||  \n  /||  \n //||\n============____________________________";
hangman[10] = "   ,==========Y===\n   ||  /      |\n   || /       |\n   ||/        O\n   ||        /|\\\n   ||          \n   ||  \n  /||  \n //||\n============____________________________";
hangman[11] = "   ,==========Y===\n   ||  /      |\n   || /       |\n   ||/        O\n   ||        /|\\\n   ||        /  \n   ||  \n  /||  \n //||\n============____________________________";
hangman[12] = "   ,==========Y===\n   ||  /      |\n   || /       |\n   ||/        O\n   ||        /|\\\n   ||        /|  \n   ||  \n  /||  \n //||\n============____________________________";
hangman[13] = "   ,==========Y===\n   ||  /       |\n   || /       |\n   ||/        O\n   ||        /|\\\n   ||        /|\n   ||                          Ah ah.\n  /||                                 O\n //||                                /|\\\n============_________________________/ \\";

// Thanks to Laurent Le Brun for the hangman ascii art : http://laurent.le-brun.eu/ascii/index.php/2007/10/10/16-pendu
